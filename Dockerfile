FROM openjdk:8-jdk-alpine
EXPOSE 8080
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
CMD ["/bin/sh"]
ENTRYPOINT ["java", "-jar", "-Duser.timezone=America/Bogota", "/app.jar"]
