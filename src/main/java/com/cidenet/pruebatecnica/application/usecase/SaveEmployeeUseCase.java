package com.cidenet.pruebatecnica.application.usecase;

import java.util.Date;
import java.util.Optional;

import com.cidenet.pruebatecnica.domain.entity.Employee;
import com.cidenet.pruebatecnica.domain.enums.CountryDomainEnum;
import com.cidenet.pruebatecnica.domain.enums.DocumentTypeEnum;
import com.cidenet.pruebatecnica.domain.exception.DomainException;
import com.cidenet.pruebatecnica.domain.repository.EmployeeRepository;

public class SaveEmployeeUseCase {

	private EmployeeRepository repository;
	
	private GetEmployeeUseCase getEmployeeUseCase;
	
	public SaveEmployeeUseCase(EmployeeRepository repository, GetEmployeeUseCase getEmployeeUseCase) {
		this.repository = repository;
		this.getEmployeeUseCase = getEmployeeUseCase;
	}
	
	public Employee create(Employee entity) {
		entity.setCreationDate(new Date());
		return save(entity, false);
	}
	
	public Employee edit(Employee entity) {
		entity.setUpdateDate(new Date());
		return save(entity, true);
	}
	
	public Employee save(Employee entity, boolean isEdit) {
		validate(entity, isEdit);
		return repository.save(entity);
	}
	
	public void validate(Employee entity, boolean isEdit) {
		if (entity.getSurname() == null || entity.getSurname().trim().isEmpty()) {
			throw new DomainException("Primer apellido es obligatorio");
		}
		if (entity.getSurname().length() > Employee.MAX_LENGTH_SURNAME) {
			throw new DomainException(String.format("Máximo caracteres para primer apellido es %d",
					Employee.MAX_LENGTH_SURNAME));
		}
		if (entity.getSecondSurname() == null || entity.getSecondSurname().trim().isEmpty()) {
			throw new DomainException("Segundo apellido es obligatorio");
		}
		if (entity.getSecondSurname().length() > Employee.MAX_LENGTH_SECOND_SURNAME) {
			throw new DomainException(String.format("Máximo caracteres para segundo apellido es %d",
					Employee.MAX_LENGTH_SECOND_SURNAME));
		}
		if (entity.getFirstName() == null || entity.getFirstName().trim().isEmpty()) {
			throw new DomainException("Primer nombre es obligatorio");
		}
		if (entity.getFirstName().length() > Employee.MAX_LENGTH_FIRST_NAME) {
			throw new DomainException(String.format("Máximo caracteres para primer nombre es %d",
					Employee.MAX_LENGTH_FIRST_NAME));
		}
		if ( entity.getOtherNames() != null && !entity.getOtherNames().trim().isEmpty()
				&& entity.getOtherNames().length() > Employee.MAX_LENGTH_OTHER_NAMES) {
			throw new DomainException(String.format("Máximo caracteres para otros nombres es %d",
					Employee.MAX_LENGTH_OTHER_NAMES));
		}
		if (entity.getDocumentType() != null && !entity.getDocumentType().trim().isEmpty()) {
			if (DocumentTypeEnum.getDocumentType(entity.getDocumentType()) == null) {
				throw new DomainException("Tipo de identificación no es válido");
			}
			if (entity.getDocumentNumber() == null || entity.getDocumentNumber().trim().isEmpty()) {
				throw new DomainException("Número de identificación es requerido");
			}
			if (entity.getDocumentNumber().length() > Employee.MAX_LENGTH_DOCUMENT_NUMBER) {
				throw new DomainException(String.format("Máximo caracteres para número de identificación es %d",
						Employee.MAX_LENGTH_DOCUMENT_NUMBER));
			}
		}
		if (entity.getDocumentNumber() != null && !entity.getDocumentNumber().trim().isEmpty()) {
			if (entity.getDocumentType() == null || entity.getDocumentType().trim().isEmpty()) {
				throw new DomainException("Tipo de identificación es requerido");
			}
		}	
		if (entity.getDocumentNumber() != null && !entity.getDocumentNumber().trim().isEmpty()) {
			if (entity.getDocumentType() != null && !entity.getDocumentType().trim().isEmpty()) {
				getEmployeeUseCase.findByDocument(entity.getDocumentType(), entity.getDocumentNumber())
				.ifPresent( e -> {
					if (!isEdit || (isEdit && !e.getId().equals(entity.getId()))) {
						throw new DomainException("Existe un empleado con la misma combinación, tipo de idetificación y número de identificación");
					} 
				});
			}
		}
		if (!isEdit || isEmailChange(entity)) {
			entity.setEmail(generateEmail(entity));
		}
		if ( entity.getEmail() != null && !entity.getEmail().trim().isEmpty()) {	
			if (entity.getEmail().length() > Employee.MAX_LENGTH_EMAIL) {
				throw new DomainException(String.format("Máximo caracteres para correo electrónico es %d",
				  Employee.MAX_LENGTH_EMAIL));
			}
		}
		
	}
	
	public String generateEmail(Employee entity) {
		if (entity.getCountry() != null && !entity.getCountry().getCode().trim().isEmpty()) {
			String username = entity.getFirstName().trim().replace(" ", "") + "." + entity.getSurname().trim().replace(" ", "");
			String domain = CountryDomainEnum.getCountryComain(entity.getCountry().getCode()).getValue();
			String email = username + "@" + domain;
			if (getEmployeeUseCase.findByEmail(email).isPresent()) {
				long count = getEmployeeUseCase.countByEmailStartsWith(username);
				username += "." + (count + 1);
				return username + "@" + domain;
			}
			return email;
		}
		return null;
	}
	
	public boolean isEmailChange(Employee entity) {
		Optional<Employee> employee = getEmployeeUseCase.findById(entity.getId());
		if (employee.isPresent() && (!employee.get().getSurname().trim().equalsIgnoreCase(entity.getSurname().trim())
				|| !employee.get().getFirstName().trim().equalsIgnoreCase(entity.getFirstName().trim()))) {
			return true;
		}
		return false;
	}
}
