package com.cidenet.pruebatecnica.application.usecase;

import java.util.Optional;

import com.cidenet.pruebatecnica.domain.entity.Employee;
import com.cidenet.pruebatecnica.domain.repository.EmployeeRepository;
import com.cidenet.pruebatecnica.infrastructure.controller.model.EmployeeResponse;
import com.cidenet.pruebatecnica.infrastructure.controller.model.PagedResponse;

public class GetEmployeeUseCase {

	private EmployeeRepository repository;
	
    public GetEmployeeUseCase(EmployeeRepository repository) {
    	this.repository = repository;
    }
    
    public Optional<Employee> findById(Integer id){
    	return repository.findById(id);
    }
    
    public Optional<Employee> findByDocument(String documentType, String documentNumber) {
    	return repository.findByDocument(documentType, documentNumber);
    }
    
    public Optional<Employee> findByEmail(String email) {
    	return repository.findByEmail(email);
    }
    
    public long countByEmailStartsWith(String email) {
    	return repository.countByEmailStartsWith(email);
    }
    
    public PagedResponse<EmployeeResponse> findAll(int page, int size) {
        return repository.findAll(page, size);
    }
}
