package com.cidenet.pruebatecnica.infrastructure.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cidenet.pruebatecnica.domain.entity.Country;
import com.cidenet.pruebatecnica.domain.entity.Department;
import com.cidenet.pruebatecnica.domain.entity.Employee;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "employee")
public class EmployeeEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String surname;
	private String secondSurname;
	private String firstName;
	private String otherNames;
	private String documentType;
	private String documentNumber;
	private String email;
	private int status;
	@Temporal(TemporalType.DATE)
	private Date admissionDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private DepartmentEntity department;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "country")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private CountryEntity country;
	
	public static EmployeeEntity from(Employee employee) {
		EmployeeEntity entity = new EmployeeEntity();
		entity.setId(employee.getId());
		entity.setSurname(employee.getSurname());
		entity.setSecondSurname(employee.getSecondSurname()); 
		entity.setFirstName(employee.getFirstName());
		entity.setOtherNames(employee.getOtherNames());
		entity.setDocumentType(employee.getDocumentType());
		entity.setDocumentNumber(employee.getDocumentNumber());
		if (employee.getCountry() != null) {
			CountryEntity country = new CountryEntity();
			country.setCode(employee.getCountry().getCode());
			entity.setCountry(country);
		}
		entity.setEmail(employee.getEmail());
		entity.setStatus(employee.getStatus());
		if (employee.getDepartment() != null) {
			DepartmentEntity department = new DepartmentEntity();
			department.setId(employee.getDepartment().getId());
			entity.setDepartment(department);
		}
		entity.setAdmissionDate(employee.getAdmissionDate());
		entity.setCreationDate(employee.getCreationDate());
		entity.setUpdateDate(employee.getUpdateDate());
		return entity;
    }
	
	public static Employee toEmployee(EmployeeEntity employee) {
        return new Employee(
                employee.getId(),
                employee.getSurname(),
                employee.getSecondSurname(),
                employee.getFirstName(),
                employee.getOtherNames(),
                employee.getDocumentType(),
                employee.getDocumentNumber(),
                employee.getCountry() != null ? 
                		new Country(employee.getCountry().getCode(), employee.getCountry().getName()) : null,
                employee.getEmail(),
                employee.getStatus(),
                employee.getDepartment() != null ?
                		new Department(employee.getDepartment().getId(), employee.getDepartment().getName()) : null,
                employee.getAdmissionDate(),
                employee.getCreationDate(),
                employee.getUpdateDate()
        );
    }
}
