package com.cidenet.pruebatecnica.infrastructure.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cidenet.pruebatecnica.infrastructure.entity.EmployeeEntity;

@Repository
public interface JpaEmployeeRepository extends JpaRepository<EmployeeEntity, Integer>{

	Optional<EmployeeEntity> findByDocumentTypeAndDocumentNumber(String documentType, String documentNumber);
	
	Optional<EmployeeEntity> findByEmail(String email);
	
	long countByEmailStartsWith(String email);
}
