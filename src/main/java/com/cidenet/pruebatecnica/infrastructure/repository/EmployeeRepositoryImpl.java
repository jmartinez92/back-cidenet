package com.cidenet.pruebatecnica.infrastructure.repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.cidenet.pruebatecnica.domain.entity.Employee;
import com.cidenet.pruebatecnica.domain.repository.EmployeeRepository;
import com.cidenet.pruebatecnica.infrastructure.controller.model.EmployeeResponse;
import com.cidenet.pruebatecnica.infrastructure.controller.model.PagedResponse;
import com.cidenet.pruebatecnica.infrastructure.entity.EmployeeEntity;

@Component
public class EmployeeRepositoryImpl implements EmployeeRepository{

	@Autowired
	private JpaEmployeeRepository repository;
	
	@Override
	public Employee save(Employee entity) {
		EmployeeEntity newEntity = repository.save(EmployeeEntity.from(entity));
		return EmployeeEntity.toEmployee(newEntity);
	}

	@Override
	public Optional<Employee> findById(Integer id) {
		Optional<EmployeeEntity> employee = repository.findById(id);
		return employee.isPresent() ? Optional.of(EmployeeEntity.toEmployee(employee.get())) : Optional.empty();
	}

	@Override
	public Optional<Employee> findByDocument(String documentType, String documentNumber) {
		Optional<EmployeeEntity> employee = repository.findByDocumentTypeAndDocumentNumber(documentType, documentNumber);
		return employee.isPresent() ? Optional.of(EmployeeEntity.toEmployee(employee.get())) : Optional.empty();
	}

	@Override
	public Optional<Employee> findByEmail(String email) {
		Optional<EmployeeEntity> employee = repository.findByEmail(email);
		return employee.isPresent() ? Optional.of(EmployeeEntity.toEmployee(employee.get())) : Optional.empty();
	}

	@Override
	public long countByEmailStartsWith(String email) {
		return repository.countByEmailStartsWith(email);
	}

	@Override
	public PagedResponse<EmployeeResponse> findAll(int page, int size){
		PageRequest pageRequest = PageRequest.of(page, size);
		Page<EmployeeEntity> pages = repository.findAll(pageRequest);
		List<EmployeeResponse> content = pages.getContent().stream().map(employee -> 
		      EmployeeResponse.from(EmployeeEntity.toEmployee(employee))).collect(Collectors.toList());
		return new PagedResponse<>(content, pages.getNumber(),
				pages.getNumberOfElements(),
				pages.getTotalElements(),
				pages.getTotalPages());
	}
	
}
