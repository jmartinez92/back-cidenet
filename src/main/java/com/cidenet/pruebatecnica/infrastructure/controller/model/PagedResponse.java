package com.cidenet.pruebatecnica.infrastructure.controller.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PagedResponse<E> {

	private List<E> content;
	private long number;
	private long numberElements;
	private long totalElements;
	private int totalPages;
}
