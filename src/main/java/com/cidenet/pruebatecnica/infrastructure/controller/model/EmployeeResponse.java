package com.cidenet.pruebatecnica.infrastructure.controller.model;

import java.util.Date;

import com.cidenet.pruebatecnica.domain.entity.Employee;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeResponse {

	private Integer id;
	private String surname;
	private String secondSurname;
	private String firstName;
	private String otherNames;
	private String documentType;
	private String documentNumber;
	private String country;
	private String countryName;
	private String email;
	private int status;
	private Integer department;
	private String departmentName;
	private Date admissionDate;
	
	public static EmployeeResponse from(Employee employee) {
        return new EmployeeResponse(
        		employee.getId(),
                employee.getSurname(),
                employee.getSecondSurname(),
                employee.getFirstName(),
                employee.getOtherNames(),
                employee.getDocumentType(),
                employee.getDocumentNumber(),
                employee.getCountry() != null ? 
                		employee.getCountry().getCode() : null,
        		employee.getCountry() != null ? 
                		employee.getCountry().getName() : null,
                employee.getEmail(),
                employee.getStatus(),
                employee.getDepartment() != null ?
                		employee.getDepartment().getId() : null,
        		employee.getDepartment() != null ?
                		employee.getDepartment().getName() : null,		
                employee.getAdmissionDate()
        );
    }
}
