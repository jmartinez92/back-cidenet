package com.cidenet.pruebatecnica.infrastructure.controller;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.pruebatecnica.application.usecase.SaveEmployeeUseCase;
import com.cidenet.pruebatecnica.application.usecase.GetEmployeeUseCase;
import com.cidenet.pruebatecnica.domain.entity.Employee;
import com.cidenet.pruebatecnica.domain.exception.DomainException;
import com.cidenet.pruebatecnica.infrastructure.controller.model.EmployeeRequest;
import com.cidenet.pruebatecnica.infrastructure.controller.model.EmployeeResponse;
import com.cidenet.pruebatecnica.infrastructure.controller.model.PagedResponse;
import com.cidenet.pruebatecnica.infrastructure.controller.model.Response;

@CrossOrigin
@RestController
@RequestMapping("/employees")
public class EmployeeController {

	private SaveEmployeeUseCase saveEmployee;
	private GetEmployeeUseCase getEmployeeUseCase;
	
	public EmployeeController(SaveEmployeeUseCase saveEmployee, GetEmployeeUseCase getEmployeeUseCase) {
		this.saveEmployee = saveEmployee;
		this.getEmployeeUseCase = getEmployeeUseCase;
	}
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<Response<PagedResponse<EmployeeResponse>>> findAll(@RequestParam("page") int page, @RequestParam("size") int size) {
		PagedResponse<EmployeeResponse> pages = getEmployeeUseCase.findAll(page, size);
		Response<PagedResponse<EmployeeResponse>> response = new Response<>(HttpStatus.OK.value(),
											 				 "Solicitud procesada correctamente", pages);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<Response<EmployeeResponse>> findById(@PathVariable Integer id) {
		Response<EmployeeResponse> response;
		Optional<Employee> entity = getEmployeeUseCase.findById(id);
		if (!entity.isPresent()){
			response = new Response<>(HttpStatus.NOT_FOUND.value(), "Registro no encontrado", null);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		response = new Response<>(HttpStatus.OK.value(), "Registro encontrado", EmployeeResponse.from(entity.get()));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
	
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response<EmployeeResponse>> create(@RequestBody EmployeeRequest request){
		Response<EmployeeResponse> response;
		try {
			Employee employee = saveEmployee.create(EmployeeRequest.toEmployee(request));
			response = new Response<>(HttpStatus.CREATED.value(),
									  "Solicitud procesada correctamente",
									  EmployeeResponse.from(employee));
		} catch (DomainException e) {
			response = new Response<>(HttpStatus.BAD_REQUEST.value(), e.getMessage(), null);
		} 
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response<EmployeeResponse>> edit(@PathVariable Integer id, @RequestBody EmployeeRequest request){
		Response<EmployeeResponse> response;
		try {
			Optional<Employee> entity = getEmployeeUseCase.findById(id);
			if (!entity.isPresent()){
				response = new Response<>(HttpStatus.NOT_FOUND.value(), "Registro no encontrado", null);
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
			request.setId(entity.get().getId());
			Employee employee = saveEmployee.edit(EmployeeRequest.toEmployee(request));
			response = new Response<>(HttpStatus.OK.value(), "Solicitud procesada correctamente", EmployeeResponse.from(employee));
		} catch (DomainException e) {
			response = new Response<>(HttpStatus.BAD_REQUEST.value(), e.getMessage(), null);
		} 
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
