package com.cidenet.pruebatecnica.infrastructure.controller.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Response<E> {
	
	private int status;
	private String message;
	private E data; 
}
