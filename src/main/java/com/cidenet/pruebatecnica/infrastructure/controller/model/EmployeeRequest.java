package com.cidenet.pruebatecnica.infrastructure.controller.model;

import java.util.Date;

import com.cidenet.pruebatecnica.domain.entity.Country;
import com.cidenet.pruebatecnica.domain.entity.Department;
import com.cidenet.pruebatecnica.domain.entity.Employee;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeRequest {

	private Integer id;
	private String surname;
	private String secondSurname;
	private String firstName;
	private String otherNames;
	private String documentType;
	private String documentNumber;
	private String country;
	private String email;
	private int status;
	private Integer department;
	private Date admissionDate;
	
	public static Employee toEmployee(EmployeeRequest employee) {
        return new Employee(
        		employee.getId(),
                employee.getSurname(),
                employee.getSecondSurname(),
                employee.getFirstName(),
                employee.getOtherNames(),
                employee.getDocumentType(),
                employee.getDocumentNumber(),
                employee.getCountry() != null && !employee.getCountry().isEmpty() ? 
                		new Country(employee.getCountry()) : null,
                employee.getEmail(),
                employee.getStatus(),
                employee.getDepartment() != null  ? 
                		new Department(employee.getDepartment()) : null,
                employee.getAdmissionDate(),
                null,
                null
        );
    }
}
