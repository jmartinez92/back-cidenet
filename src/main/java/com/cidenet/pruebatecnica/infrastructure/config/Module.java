package com.cidenet.pruebatecnica.infrastructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cidenet.pruebatecnica.application.usecase.SaveEmployeeUseCase;
import com.cidenet.pruebatecnica.application.usecase.GetEmployeeUseCase;
import com.cidenet.pruebatecnica.domain.repository.EmployeeRepository;

@Configuration
public class Module {

	@Bean 
	public GetEmployeeUseCase getEmployeeUseCase(EmployeeRepository repository) {
		return new GetEmployeeUseCase(repository);
	}
	@Bean
    public SaveEmployeeUseCase createEmployeeUserCase(EmployeeRepository repository, GetEmployeeUseCase getEmployeeUseCase) {
        return new SaveEmployeeUseCase(repository, getEmployeeUseCase);
    }
}
