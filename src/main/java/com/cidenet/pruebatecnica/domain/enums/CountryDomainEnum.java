package com.cidenet.pruebatecnica.domain.enums;

import java.util.Arrays;

import lombok.Getter;

@Getter
public enum CountryDomainEnum {
	
	COLOMBIA("169", "cidenet.com.co"),
	USA("249", "cidenet.com.us");
	
	private String code;
	private String value;
	
	CountryDomainEnum(String code, String value) {
		this.code = code;
		this.value = value;
	}

	public static CountryDomainEnum getCountryComain(String code) {
		return Arrays.stream(CountryDomainEnum.values()).filter(v -> v.getCode().equals(code)).findFirst().orElse(null);
	}
}
