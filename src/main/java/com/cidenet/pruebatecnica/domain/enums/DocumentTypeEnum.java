package com.cidenet.pruebatecnica.domain.enums;

import java.util.Arrays;

public enum DocumentTypeEnum {
	
	CC, CE, PA, PE;
	
	public static DocumentTypeEnum getDocumentType(String code) {
		return Arrays.stream(DocumentTypeEnum.values()).filter(v -> v.name().equals(code)).findFirst().orElse(null);
	}
}
