package com.cidenet.pruebatecnica.domain.repository;

import java.util.Optional;

import com.cidenet.pruebatecnica.domain.entity.Employee;
import com.cidenet.pruebatecnica.infrastructure.controller.model.EmployeeResponse;
import com.cidenet.pruebatecnica.infrastructure.controller.model.PagedResponse;

public interface EmployeeRepository {

	public Employee save(Employee entity);
	
	public Optional<Employee> findById(Integer id);
		
	public Optional<Employee> findByDocument(String documentType, String documentNumber);
	
	public Optional<Employee> findByEmail(String email);
	
	public long countByEmailStartsWith(String email);
	
	public PagedResponse<EmployeeResponse> findAll(int page, int size);
}
