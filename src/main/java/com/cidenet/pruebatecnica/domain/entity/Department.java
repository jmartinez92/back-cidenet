package com.cidenet.pruebatecnica.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Department {

	private Integer id;
	private String name;
	
	public Department(Integer id) {
		this.id = id;
	}
}
