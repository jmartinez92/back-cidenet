package com.cidenet.pruebatecnica.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Country {

	private String code;
	private String name;
	
	public Country(String code) {
		this.code = code;
	}
}
