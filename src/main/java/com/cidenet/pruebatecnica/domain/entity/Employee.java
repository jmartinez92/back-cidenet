package com.cidenet.pruebatecnica.domain.entity;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

	private Integer id;
	private String surname;
	private String secondSurname;
	private String firstName;
	private String otherNames;
	private String documentType;
	private String documentNumber;
	private Country country;
	private String email;
	private int status;
	private Department department;
	private Date admissionDate;
	private Date creationDate;
	private Date updateDate;
	
	public final static int MAX_LENGTH_SURNAME = 20;
	public final static int MAX_LENGTH_SECOND_SURNAME = 20;
	public final static int MAX_LENGTH_FIRST_NAME = 20;
	public final static int MAX_LENGTH_OTHER_NAMES = 50;
	public final static int MAX_LENGTH_DOCUMENT_NUMBER = 20;
	public final static int MAX_LENGTH_EMAIL = 300;
}
