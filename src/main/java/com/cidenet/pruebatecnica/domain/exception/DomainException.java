package com.cidenet.pruebatecnica.domain.exception;

public class DomainException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 7557016991911289567L;

	public DomainException(String message) {
        super(message);
    }
}
