package com.cidenet.pruebatecnica.application.usecase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.cidenet.pruebatecnica.domain.entity.Country;
import com.cidenet.pruebatecnica.domain.entity.Department;
import com.cidenet.pruebatecnica.domain.entity.Employee;
import com.cidenet.pruebatecnica.domain.repository.EmployeeRepository;

@RunWith(MockitoJUnitRunner.class)
public class SaveEmployeeUseCaseTest {

	@Mock
	private EmployeeRepository repository;
	
	@InjectMocks
	private GetEmployeeUseCase getCase = new GetEmployeeUseCase(repository);
	
	@Spy
	@InjectMocks
	private SaveEmployeeUseCase service = new SaveEmployeeUseCase(repository, getCase);
	
	private static final Integer id = 1; 
	private static final String surname = "PRUEBA";
	private static final String secondSurname = "PRUEBA";
	private static final String firstName = "PRUEBA";
	private static final String otherNames = "PREUBA";
	private static final String documentType = "CC";
	private static final String documentNumber = (new Random().nextInt(9000000) + 1000000) + ""; 
	private static final String country = "169";
	private static final int status = 1;
	private static final Integer department = 1;
	private static final Date admissionDate = new Date();
	 
	
	@Before
    public void init() {
		
		Employee employee = new Employee();
		employee.setSurname(surname);
		employee.setSecondSurname(secondSurname);
		employee.setFirstName(firstName);
		employee.setOtherNames(otherNames);
		employee.setDocumentType(documentType);
		employee.setDocumentNumber(documentNumber);
		employee.setCountry(new Country(country));
		employee.setDepartment(new Department(department));
		employee.setAdmissionDate(admissionDate);
		employee.setStatus(status);
		
		doReturn(UUID.randomUUID() + "@cidenet.com.co").when(service).generateEmail(any(Employee.class));
		String email = service.generateEmail(employee);
		
        when(repository.findByDocument(documentType, documentNumber)).thenReturn(Optional.empty());
        when(repository.findById(id)).thenReturn(Optional.of(employee));
        when(repository.findByEmail(email)).thenReturn(Optional.empty());
        when(repository.countByEmailStartsWith("Hola@hotmail.com")).thenReturn(1L);
        when(repository.save(any(Employee.class))).thenReturn(employee);
 
    }
	
	@Test
	public void testCreate() {
		Employee employee = new Employee();
		employee.setSurname(surname);
		employee.setSecondSurname(secondSurname);
		employee.setFirstName(firstName);
		employee.setOtherNames(otherNames);
		employee.setDocumentType(documentType);
		employee.setDocumentNumber(documentNumber);
		employee.setCountry(new Country(country));
		employee.setDepartment(new Department(department));
		employee.setAdmissionDate(admissionDate);
		employee.setStatus(status);
		
		Employee entity = service.create(employee);
		assertEquals(surname, entity.getSurname());
	}
	
	@Test
	public void testEdit() {
		Employee employee = new Employee();
		employee.setId(id);
		employee.setSurname("MACIAS");
		employee.setSecondSurname(secondSurname);
		employee.setFirstName(firstName);
		employee.setOtherNames(otherNames);
		employee.setDocumentType(documentType);
		employee.setDocumentNumber(documentNumber);
		employee.setCountry(new Country(country));
		employee.setDepartment(new Department(department));
		employee.setAdmissionDate(admissionDate);
		employee.setStatus(status);
		
		Employee entity = service.edit(employee);
		assertTrue(firstName.equals(entity.getFirstName()));
	}


}
